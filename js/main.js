/*jshint browser:true*/
/*globals Ticker:true*/

(function () {
    'use strict';

    var ticker = new Ticker();

    document.querySelector("#tickStartButton").addEventListener('click', function (e) {
        e.stopPropagation();

        var precision = document.querySelector("#tickPrecisionSelect").value;
        var interval = document.querySelector("#tickIntervalInput").value;

        ticker.interval = +interval;
        ticker.precision = precision;
        ticker.timerElement = document.querySelector("#countdownTimer");
        ticker.start();

        return false;
    });

    document.querySelector("#tickStopButton").addEventListener('click', function (e) {
        ticker.stop();
    });
}());
