/*jshint browser:true*/

function Ticker() {
    'use strict';

    this.interval = 0;

    this.precision = "s"; //1: ms; 1000: s; 60000: m
    this._precisionMap = {
        "ms": 1,
        "s": 1000,
        "min": 60000
    };

    this.timerElement = null;

    this.countDown = false; //false to count up, true to count down

    this._tickReference = null;
    this._lastTick = -1;
    this._tickStatus = 0;

    this.completeFn = function () {};

    Object.defineProperties(this, {
        "_actualPrecision": {
            get: function () {
                return this._precisionMap[this.precision];
            }
        },
        "_upperBound": {
            get: function () {
                return (this.interval * this._actualPrecision);
            }
        }
    });
}

Ticker.prototype.start = function () {
    'use strict';

    this.reset();
    this._lastTick = window.performance.now();
    this.tick(this._actualPrecision);
};

Ticker.prototype.stop = function () {
    'use strict';

    cancelAnimationFrame(this._tickReference);
    this._tickReference = null;
};

Ticker.prototype.reset = function () {
    'use strict';

    this._tickReference = null;
    this._tickStatus = 0;
    this._lastTick = null;
};

Ticker.prototype.tick = function () {
    'use strict';

    var timeLapsed = window.performance.now() - this._lastTick;
    this._lastTick = window.performance.now();
    this._tickStatus += timeLapsed;

    this.updateCountdown();

    var self = this;
    if (this._tickStatus < this._upperBound)
        this._tickReference = requestAnimationFrame(function () {
            self.tick();
        });
    else
        this.completeFn();
};

Ticker.prototype.updateCountdown = function () {
    'use strict';

    var value = this._tickStatus;

    if (this.countDown)
        value = this._upperBound - this._tickStatus;

    if (value > this._upperBound)
        value = this._upperBound;

    var ms = Math.floor(value % 1000),
        s = Math.floor((value / 1000) % 60),
        m = Math.floor((value / 1000) / 60);

    //keep them from dipping past 0 on countdown
    ms = ms < 0 ? 0 : ms;
    s = s < 0 ? 0 : s;
    m = m < 0 ? 0 : m;

    ms = ("000" + ms).slice(-3);
    s = ("00" + s).slice(-2);

    var output = m + ":" + s + "." + ms;

    this.timerElement.innerHTML = output;
};
